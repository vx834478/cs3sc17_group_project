#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#define CMD_LEN 128

int create_folder(char *folder_name)
{
	if(!folder_name)
	{
		return -1;
	}
	
	char cmd[CMD_LEN] = {0};
	sprintf(cmd,"mkdir %s",folder_name);
	system(cmd);
	
	memset(cmd,0,CMD_LEN);
	sprintf(cmd,"mkdir %s/bin",folder_name);
	system(cmd);
	
	memset(cmd,0,CMD_LEN);
	sprintf(cmd,"mkdir %s/docs",folder_name);
	system(cmd);
	
	memset(cmd,0,CMD_LEN);
	sprintf(cmd,"mkdir %s/lib",folder_name);
	system(cmd);
	
	memset(cmd,0,CMD_LEN);
	sprintf(cmd,"mkdir %s/src",folder_name);
	system(cmd);
	
	memset(cmd,0,CMD_LEN);
	sprintf(cmd,"mkdir %s/tests",folder_name);
	system(cmd);
	return 0;
}

int git_cmd(char *cmd,char *folder_name)
{
	if(!cmd || !folder_name)
	{
		return -1;
	}
	
	char tem_cmd[CMD_LEN] = {0};
	sprintf(tem_cmd,"cd %s&&%s",folder_name,cmd);
	system(tem_cmd);
	return 0;
}

void find_tag(char *path,char *tag_name)
{
    DIR *pDir;
    struct dirent *ent;
    char child_path[128] = {0};
 
    pDir = opendir(path); 
    while ((ent = readdir(pDir)) != NULL) {
        if (ent->d_type & DT_DIR) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            sprintf(child_path, "%s/%s", path, ent->d_name);
			char file_name[128] = {0};
			sprintf(file_name,"%s/.pm_tag",child_path);
			if(access(file_name,F_OK)==0)
			{
				FILE *fp = fopen(file_name,"r");
				if(fp)
				{
					char content[128] = {0};
					fread(content,1,128,fp);
					fclose(fp);
					char *tmp = NULL;
					if((tmp = strstr(content, "\n")))
					{
						*tmp = '\0';
					}
					if(strcmp(content,tag_name)==0)
					{
						printf("%s\n",child_path);
						exit(0);
					}
				}
			}
            find_tag(child_path,tag_name);
        }
    }
}

int get_path(char *path,char *tag_name,char *find_path)
{
	DIR *pDir;
    struct dirent *ent;
    char child_path[128] = {0};
 
    pDir = opendir(path); 
    while ((ent = readdir(pDir)) != NULL) {
        if (ent->d_type & DT_DIR) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            sprintf(child_path, "%s/%s", path, ent->d_name);
			char file_name[128] = {0};
			sprintf(file_name,"%s/.pm_tag",child_path);
			if(access(file_name,F_OK)==0)
			{
				FILE *fp = fopen(file_name,"r");
				if(fp)
				{
					char content[128] = {0};
					fread(content,1,128,fp);
					fclose(fp);
					char *tmp = NULL;
					if((tmp = strstr(content, "\n")))
					{
						*tmp = '\0';
					}
					if(strcmp(content,tag_name)==0)
					{
						strcpy(find_path,child_path);
						return 0;
					}
				}
			}
            get_path(child_path,tag_name,find_path);
        }
    }
	return 0;
}

int main(int argc, char **argv)
{
	if(argc<2)
	{
		printf("bad arg\n");
		return -1;
	}
	
	if(strcmp(argv[1],"create_project")==0)
	{
		if(argc!=3)
		{
			printf("Wrong number of parameters, should be pm command arg");
			return -1;
		}
		if(strstr(argv[2]," "))
		{
			printf("Bad characters in folder name\n");
			return -1;
		}
		if(opendir(argv[2])!=NULL)
		{
			printf("A folder of that name already exists.  Aborting.\n");
			return -1;
		}
		create_folder(argv[2]);
		git_cmd("git init",argv[2]);
	}
	else if(strcmp(argv[1],"add_feature")==0)
	{
		if(argc!=3)
		{
			printf("Wrong number of parameters, should be pm command arg");
			return -1;
		}
		if(strstr(argv[2]," "))
		{
			printf("Bad characters in folder name\n");
			return -1;
		}
		if(opendir(argv[2])!=NULL)
		{
			printf("A folder of that name already exists.  Aborting.\n");
			return -1;
		}
		create_folder(argv[2]);
		char cmd[CMD_LEN] = {0};
		sprintf(cmd,"git branch %s",argv[2]);
		system(cmd);
	}
	else if(strcmp(argv[1],"add_tag")==0)
	{
		if(argc!=3)
		{
			printf("Wrong number of parameters, should be pm command arg");
			return -1;
		}
		if(strstr(argv[2]," "))
		{
			printf("Bad characters in folder name\n");
			return -1;
		}
		if(access(".pm_tag",F_OK)==0)
		{
			printf("Already a tag for this folder\n");
			system("cat .pm_tag");
		}
		else
		{
			FILE *fp = fopen(".pm_tag","w");
			if(fp)
			{
				fwrite(argv[2],strlen(argv[2]),1,fp);
				fclose(fp);
			}
		}
	}
	else if(strcmp(argv[1],"find_tag")==0)
	{
		if(argc!=3)
		{
			printf("Wrong number of parameters, should be pm command arg");
			return -1;
		}
		if(strstr(argv[2]," "))
		{
			printf("Bad characters in folder name\n");
			return -1;
		}
		find_tag(".",argv[2]);
	}
	else if(strcmp(argv[1],"move_by_tag")==0)
	{
		if(argc!=4)
		{
			printf("Wrong number of parameters, should be pm command arg");
			return -1;
		}
		if(strstr(argv[2]," ")||strstr(argv[3]," "))
		{
			printf("Bad characters in folder name\n");
			return -1;
		}
		char src[128] = {0};
		char dst[128] = {0};
		get_path(".",argv[2],src);
		get_path(".",argv[3],dst);
		char cmd[128] = {0};
		sprintf(cmd,"mv %s %s",src,dst);
		system(cmd);
	}
	return 0;
}
